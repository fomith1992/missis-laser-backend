import { Injectable, CanActivate, ExecutionContext, HttpStatus } from '@nestjs/common'
import { Request as RequestExpress } from 'express'
import { Observable } from 'rxjs'

import { AuthBodyDto } from './auth-body.dto'
import { BaseException } from '../../exceptions/base.exception'
import { YclientsService } from '../../../services/yclients/yclients.service'

type Request = RequestExpress<any, AuthBodyDto, AuthBodyDto>

@Injectable()
export class AuthYclientsGuard implements CanActivate {
  constructor(private yclientsService: YclientsService) {}

  canActivate(context: ExecutionContext): boolean | Promise<boolean> | Observable<boolean> {
    const request = context.switchToHttp().getRequest<Request>()

    return this.validateRequest(request)
  }

  private async validateRequest(request: Request) {
    if (request.body?.userToken && request.body?.phone) {
      const { phone, userToken } = request.body

      return await this.yclientsService.verifyYclientsUser({ phone, userToken })
    }

    throw new BaseException({ message: 'Not authorized', code: HttpStatus.UNAUTHORIZED })
  }
}
