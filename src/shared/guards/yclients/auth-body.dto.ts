import { IsString, Matches } from 'class-validator'

export class AuthBodyDto {
  @IsString()
  @Matches(/^7[\d]{10}$/, { message: 'Invalid phone format' })
  phone: string

  @IsString()
  userToken: string
}
