import { IsString } from 'class-validator'

export class AdminAuthBodyDto {
  @IsString()
  username?: string

  @IsString()
  password?: string
}
