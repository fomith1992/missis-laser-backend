import { Injectable, CanActivate, ExecutionContext, HttpStatus } from '@nestjs/common'
import { Request as RequestExpress } from 'express'
import { Observable } from 'rxjs'

import bcrypt from 'bcrypt'

import { BaseException } from '../../exceptions/base.exception'
import { UserService } from '../../../services/user/user.service'
import { UserRole } from '../../../services/user/schemas/user.schema'
import { AdminAuthBodyDto } from './admin-auth.dto'

type Request = RequestExpress<any, AdminAuthBodyDto, AdminAuthBodyDto>

@Injectable()
export class AdminAuthGuard implements CanActivate {
  constructor(private userService: UserService) {}

  canActivate(context: ExecutionContext): boolean | Promise<boolean> | Observable<boolean> {
    const request = context.switchToHttp().getRequest<Request>()

    return this.validateRequest(request)
  }

  private async validateRequest(request: Request) {
    if (!request.body?.username || !request.body?.password) {
      throw new BaseException({ message: 'Username and password required', code: HttpStatus.UNAUTHORIZED })
    }

    const { username, password } = request.body

    const user = await this.userService.getUserByUsername(username)
    if (!user) {
      throw new BaseException({ message: 'User not found', code: HttpStatus.UNAUTHORIZED })
    }

    if (user.role !== UserRole.Admin) {
      throw new BaseException({ message: 'Denied', code: HttpStatus.UNAUTHORIZED })
    }

    if (user?.password) {
      const isEqual = await bcrypt.compare(password, user.password)
      if (!isEqual) {
        throw new BaseException({ message: 'Username or password incorrect', code: HttpStatus.UNAUTHORIZED })
      }
    }

    return true
  }
}
