import { Maybe } from '../../utilityTypes'

export interface IHookService {
  id: number
  title: string
  cost: number
}

export interface IService {
  id: number
  title: string
  category_id: number
  price_min: number
  price_max: number
  discount: number
  prepaid: string
}

export interface IFrontService extends IService {
  imageType: Maybe<string>
  categoryName: Maybe<string>
}
