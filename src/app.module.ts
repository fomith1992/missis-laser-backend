import { Module } from '@nestjs/common'
import { ConfigModule } from '@nestjs/config'
import { MongooseModule } from '@nestjs/mongoose'
import { ScheduleModule as CronModule } from '@nestjs/schedule'
import { APP_FILTER } from '@nestjs/core'

import { NotificationModule } from './services/notification/notification.module'
import { HookReciverModule } from './services/hook-reciver/hook-reciver.module'
import { PromocodeModule } from './services/promocode/promocode.module'
import { ScheduleModule } from './services/schedule/schedule.module'
import { YclientsModule } from './services/yclients/yclients.module'
import { LandingModule } from './services/landing/landing.module'
import { UserModule } from './services/user/user.module'
import { AdminModule } from './services/admin/admin.module'

import { AllExceptionsFilter } from './shared/filtres/all-exception.filter'
import { Maybe } from './shared/utilityTypes'
import { CardsModule } from 'services/cards/cards.module'

const NODE_ENV = process.env.NODE_ENV as Maybe<'prod' | 'dev'>
@Module({
  imports: [
    ConfigModule.forRoot({ envFilePath: NODE_ENV == null ? '.env.dev' : `.env.${NODE_ENV}` }),
    CronModule.forRoot(),
    MongooseModule.forRoot(String(process.env.MONGO_CONNECTION_URL), { useFindAndModify: false, useCreateIndex: true }),

    AdminModule,
    UserModule,
    ScheduleModule,
    YclientsModule,
    PromocodeModule,
    NotificationModule,
    HookReciverModule,
    LandingModule,
    CardsModule,
  ],
  providers: [
    {
      provide: APP_FILTER,
      useClass: AllExceptionsFilter,
    },
  ],
})
export class AppModule {}
