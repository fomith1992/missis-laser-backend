import { Body, Controller, Post, UseFilters, UseGuards, Param, Query, Patch, Delete } from '@nestjs/common'
import { ApiCreatedResponse, ApiOperation, ApiQuery, ApiTags } from '@nestjs/swagger'

import { MongoExceptionFilter } from '../../shared/filtres/mongo-exception.filter'
import { ValidationFilter } from '../../shared/filtres/validation.filter'
import { AdminAuthGuard } from '../../shared/guards/admin/admin-auth.guard'
import { PromocodeCreateDto } from '../../services/promocode/dto'
import { LandingDto } from '../landing/dto/landing.dto'
import { UserDto } from '../../services/user/dto'

import { PromocodeService } from '../promocode/promocode.service'
import { LandingService } from '../landing/landing.service'
import { CardsService } from '../cards/cards.service'

import { getUserQueryDto } from './dto/get-user.query'
import { AdminService } from './admin.service'
import { CreateCardDto } from 'services/cards/dto/create.card.dto'
import { CardPriceSchema } from 'services/cards/schema/price.schema'

@Controller('admin')
@UseGuards(AdminAuthGuard)
@UseFilters(new ValidationFilter(), MongoExceptionFilter)
export class AdminController {
  constructor(
    private promocodeService: PromocodeService,
    private landingService: LandingService,
    private adminService: AdminService,
    private cardsService: CardsService,
  ) {}

  @ApiTags('landing')
  @ApiOperation({ summary: 'create landing with unique [type]' })
  @Post('landing')
  @ApiCreatedResponse({ type: LandingDto })
  async createLanding(@Body() landing: LandingDto) {
    return this.landingService.createLanding(landing)
  }

  @ApiTags('promocode')
  @ApiOperation({ summary: 'create promocode with unique [key]' })
  @Post('promocode')
  @ApiCreatedResponse({ type: PromocodeCreateDto })
  async createPromocode(@Body() promocode: PromocodeCreateDto) {
    return this.promocodeService.create(promocode)
  }

  @ApiTags('promocode')
  @ApiOperation({ summary: 'reset all activated phones' })
  @Post('promocode/reset/:key')
  async resetPhonePromocode(@Param('key') key: string) {
    return this.promocodeService.reset(key)
  }

  @Post('user')
  @ApiTags('user')
  @ApiOperation({ summary: 'get full user info' })
  @ApiQuery({ name: 'phone', required: false })
  @ApiQuery({ name: 'userId', required: false })
  @ApiCreatedResponse({ type: UserDto })
  async getUserInfo(@Query() queries: getUserQueryDto) {
    return await this.adminService.getUserByQuery(queries)
  }

  @Post('cards')
  @ApiTags('cards')
  @ApiOperation({ summary: 'create new card unique [cardId]' })
  @Post('cards')
  async createCard(@Body() card: CreateCardDto) {
    await this.cardsService.createCard(card)
  }

  @Patch('cards')
  @ApiTags('cards')
  @ApiOperation({ summary: 'patch card by cardId' })
  @ApiQuery({ name: 'cardId', required: true })
  patchCard() {
    this.cardsService.patchCard()
  }

  @Delete('cards')
  @ApiTags('cards')
  @ApiOperation({ summary: 'delete card by cardId' })
  @ApiQuery({ name: 'cardId', required: true })
  deleteCard() {
    this.cardsService.deleteCard()
  }
}
