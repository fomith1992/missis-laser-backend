import { Injectable } from '@nestjs/common'

import { UserService } from '../../services/user/user.service'
import { BaseException } from '../../shared/exceptions/base.exception'
import { getUserQueryDto } from './dto/get-user.query'

@Injectable()
export class AdminService {
  constructor(private userService: UserService) {}

  async getUserByQuery(queries: getUserQueryDto) {
    if (!queries) {
      return { success: false, message: 'Not give any query(phone/userId)' }
    }

    let user
    if (queries.userId) user = await this.userService.getUserById(queries.userId)
    if (queries.phone && !user) user = await this.userService.getUserByUsername(queries.phone)

    //Errors
    if (!user) throw new BaseException({ message: 'User not found' })

    return user
  }
}
