import { Module } from '@nestjs/common'

import { AdminController } from './admin.controller'
import { AdminService } from './admin.service'
import { PromocodeModule } from '../promocode/promocode.module'
import { LandingModule } from '../landing/landing.module'
import { UserModule } from '../user/user.module'
import { CardsModule } from '../cards/cards.module'

@Module({
  imports: [UserModule, PromocodeModule, LandingModule, CardsModule],
  controllers: [AdminController],
  providers: [AdminService],
})
export class AdminModule {}
