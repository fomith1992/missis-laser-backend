import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose'
import { Document } from 'mongoose'

import { ICardPrice } from '../interfaces'
import { CardPriceSchema } from './price.schema'

export type CardDocument = CardMongo & Document

@Schema({ collection: 'cards' })
export class CardMongo {
  @Prop({ required: true, unique: true })
  cardId: number

  @Prop({ required: true })
  title: string

  @Prop({ required: true })
  important: boolean

  @Prop({ required: true })
  type: string

  @Prop({ required: true })
  discountText: string

  @Prop({ required: true, type: CardPriceSchema })
  price: ICardPrice
}

export const CardSchema = SchemaFactory.createForClass(CardMongo)
