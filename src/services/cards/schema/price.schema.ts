import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose'

@Schema()
export class CardPrice {
  @Prop({ required: true })
  total: number

  @Prop({ required: true })
  currency: string

  @Prop()
  discount?: number

  @Prop()
  isFrom?: boolean
}

export const CardPriceSchema = SchemaFactory.createForClass(CardPrice)
