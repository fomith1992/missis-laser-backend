import { Body, Controller, ParseArrayPipe, ParseIntPipe, Post, Query, UseFilters, UseGuards } from '@nestjs/common'
import { ApiOperation, ApiQuery, ApiTags } from '@nestjs/swagger'

import { AuthYclientsGuard } from '../../shared/guards/yclients/auth-yclients.guard'
import { ValidationFilter } from '../../shared/filtres/validation.filter'
import { CardsService } from './cards.service'
import { getCardsQueryDto } from './dto/get-cards.query.dto'
import { ArrayOfNumbersQueryPipe } from 'shared/pipes/array-of-numbers-query.pipe'

@ApiTags('cards')
@Controller('cards')
@UseFilters(new ValidationFilter())
@UseGuards(AuthYclientsGuard)
export class CardsController {
  constructor(private cardsService: CardsService) {}

  @Post()
  async getCurrentActiveCards(@Query('ids', ArrayOfNumbersQueryPipe) ids: number[]) {
    return this.cardsService.getCards(ids)
  }
}
