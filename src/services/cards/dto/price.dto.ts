import { IsBoolean, IsNumber, IsOptional, IsString } from 'class-validator'

export class CardPriceDto {
  @IsNumber()
  readonly total: number

  @IsString()
  readonly currency: string

  @IsNumber()
  @IsOptional()
  readonly discount?: number

  @IsBoolean()
  @IsOptional()
  readonly isFrom?: boolean
}
