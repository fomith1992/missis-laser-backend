import { PickType } from '@nestjs/swagger'
import { CreateCardDto } from './create.card.dto'
import { IsNumber, IsNumberString, IsArray } from 'class-validator'
import { Type } from 'class-transformer'

export class getCardsQueryDto {
  @IsArray()
  @Type(() => Number)
  ids: number[]
}
