import { Type } from 'class-transformer'
import { IsNumber, IsString, IsBoolean, ValidateNested, IsDefined, IsNotEmptyObject } from 'class-validator'
import { CardPriceDto } from './price.dto'

export class CreateCardDto {
  @IsNumber()
  readonly cardId: number

  @IsString()
  readonly title: string

  @IsDefined()
  @IsNotEmptyObject()
  @ValidateNested()
  @Type(() => CardPriceDto)
  readonly price: CardPriceDto

  @IsBoolean()
  readonly important: boolean

  @IsString()
  readonly type: string

  @IsString()
  readonly discountText: string
}
