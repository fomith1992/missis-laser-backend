export interface ICardPrice {
  total: number
  currency: string

  discount?: number
  isFrom?: boolean
}

export interface ICard {
  cardId: number
  title: string
  important: boolean
  type: string
  discountText: string
  price: ICardPrice
}
