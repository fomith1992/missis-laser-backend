import { Module } from '@nestjs/common'
import { MongooseModule } from '@nestjs/mongoose'
import { CardsController } from './cards.controller'
import { CardsService } from './cards.service'
import { CardMongo, CardSchema } from './schema/cards.schema'

@Module({
  imports: [MongooseModule.forFeature([{ name: CardMongo.name, schema: CardSchema }])],
  controllers: [CardsController],
  providers: [CardsService],
  exports: [CardsService],
})
export class CardsModule {}
