import { Injectable } from '@nestjs/common'
import { InjectModel } from '@nestjs/mongoose'
import { Model } from 'mongoose'
import { ICard } from './interfaces'
import { CardMongo, CardDocument } from './schema/cards.schema'

@Injectable({})
export class CardsService {
  private projection = { 'price._id': 0, 'price.__id': 0, __v: 0, _id: 0 }

  constructor(@InjectModel(CardMongo.name) private cardMongo: Model<CardDocument>) {}

  //CRUD for admin
  async createCard(card: ICard) {
    return await this.cardMongo.create(card)
  }

  deleteCard() {
    //pass
  }

  patchCard() {
    //pass
  }

  async getCards(cards: number[]) {
    return await this.cardMongo.find({ cardId: { $in: cards } }, this.projection)
  }
}
