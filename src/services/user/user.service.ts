import { Injectable } from '@nestjs/common'
import { InjectModel } from '@nestjs/mongoose'
import { LeanDocument, Model } from 'mongoose'
import bcrypt from 'bcrypt'

import { User, UserDocument, UserRole } from './schemas/user.schema'
import { ResetPasswordParametrs, SetUserTokenParameters } from './interfaces'
import { UserDto } from './dto/user.dto'
import { UpdateUserDataDto } from './dto'

import { BaseException } from '../../shared/exceptions/base.exception'
import { clearPhone } from '../../shared/common/user'
import { Maybe } from '../../shared/utilityTypes'

@Injectable()
export class UserService {
  constructor(@InjectModel(User.name) private userModel: Model<UserDocument>) {}

  async setUserExpoToken(user: SetUserTokenParameters) {
    const maybeUser = await this.getUserByPhone(user.phone)

    if (maybeUser) {
      return await this.userModel.findOneAndUpdate(
        { username: user.phone },
        { pushToken: user.pushToken },
        { new: true },
      )
    }

    return await this.createUser({ ...user, role: UserRole.User, username: user.phone })
  }

  async getUserByPhone(phone?: string): Promise<Maybe<User>> {
    if (!phone) {
      throw new BaseException({ message: 'User not found' })
    }
    return await this.userModel.findOne({ phone: clearPhone(phone) })
  }

  async getUserByUsername(username: string): Promise<Maybe<User>> {
    return await this.userModel.findOne({ username })
  }

  async getUserById(id: string): Promise<Maybe<User>> {
    return await this.userModel.findById(id)
  }

  async getPublicDataById(id: string) {
    const user = await this.userModel.findById(id).lean()
    if (!user) {
      throw new BaseException({ message: 'User not found' })
    }

    return this.transformUserDataToPublicData(user)
  }

  async getPublicDataByUserName(username: string) {
    const user = await this.userModel.findOne({ username }).lean()
    if (!user) {
      throw new BaseException({ message: 'User not found' })
    }

    return this.transformUserDataToPublicData(user)
  }

  async getUsersByPhones(phones: string[]) {
    const clearPhones = phones.map(clearPhone)

    return await this.userModel.find({ phone: { $in: clearPhones } })
  }

  async createUser(user: UserDto) {
    if (user.password) {
      user.password = await this.hashPassword(user.password)
    }

    await this.userModel.create(user)

    return {
      success: true,
      message: 'Registration successful',
    }
  }

  async updateUser({ username, ...updateData }: UpdateUserDataDto) {
    if (Object.entries(updateData).length === 0) {
      throw new BaseException({ message: 'User data is empty, nothing to update' })
    }

    const user = await this.userModel.findOneAndUpdate(
      { phone: updateData.phone },
      { username, ...updateData },
      { new: true },
    )
    if (!user) {
      throw new BaseException({ message: 'User not found' })
    }

    return {
      success: true,
      message: 'User update successful',
    }
  }

  async resetPassword({ userId, oldPassword, newPassword }: ResetPasswordParametrs) {
    const user = await this.userModel.findById(userId)
    if (!user) throw new BaseException({ message: 'User not found' })

    if (user.password) {
      const isCorrectPassword = await bcrypt.compare(oldPassword, user.password)
      if (!isCorrectPassword) throw new BaseException({ message: 'Incorrect password' })
    }

    const newHashPassword = await this.hashPassword(newPassword)

    await user.update({ password: newHashPassword })
    return { success: true }
  }

  private async transformUserDataToPublicData(user: LeanDocument<UserDocument>) {
    const { __v, role, password, _id, ...publicData } = user

    return {
      id: _id,
      ...publicData,
    }
  }

  private async hashPassword(password: string) {
    return await bcrypt.hash(password, 12)
  }
}
