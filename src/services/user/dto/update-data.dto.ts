import { OmitType } from '@nestjs/swagger'
import { UserDto } from './user.dto'

export class UpdateUserDataDto extends OmitType(UserDto, ['role', 'password'] as const) {}
