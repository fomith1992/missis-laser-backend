import { PickType, OmitType } from '@nestjs/swagger'
import { UserDto } from './user.dto'

export class GetUserDataDto extends PickType(UserDto, ['username'] as const) {}

export class GetUserDataByPhoneDto extends PickType(UserDto, ['phone'] as const) {}

export class PublicUserDto extends OmitType(UserDto, ['password', 'role'] as const) {}
