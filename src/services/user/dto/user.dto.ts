import { IsNotEmpty, IsString, Matches, IsOptional, IsEnum } from 'class-validator'

import { Sex, UserRole } from '../schemas/user.schema'

export class UserDto {
  @IsString()
  @IsNotEmpty()
  username: string

  @IsNotEmpty()
  @IsEnum(UserRole)
  role: UserRole

  @IsString()
  @IsNotEmpty()
  @IsOptional()
  password?: string

  @IsNotEmpty()
  @IsEnum(Sex)
  @IsOptional()
  sex?: Sex

  /* Yclients */

  @IsString()
  @Matches(/^7[\d]{10}$/, { message: 'Invalid phone format' })
  @IsOptional()
  phone?: string

  @IsString()
  @IsNotEmpty()
  @IsOptional()
  pushToken?: string

  @IsString()
  @IsNotEmpty()
  @IsOptional()
  userToken?: string
}
