import { OmitType } from '@nestjs/swagger'
import { UserDto } from './user.dto'

export class RegisterUserDto extends OmitType(UserDto, ['role'] as const) {}
