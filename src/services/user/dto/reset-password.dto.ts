import { IsNotEmpty, IsString } from 'class-validator'

export class ResetPasswordDto {
  @IsString()
  @IsNotEmpty()
  old: string

  @IsString()
  @IsNotEmpty()
  new: string
}
