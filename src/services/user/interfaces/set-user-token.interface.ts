import { RegisterUserParameters } from './register-user.interface'
import { UpdateTokenParameters } from './update-token.interface'

export type SetUserTokenParameters = RegisterUserParameters & UpdateTokenParameters
