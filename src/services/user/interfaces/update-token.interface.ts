export interface UpdateTokenParameters {
  phone: string
  pushToken: string
}
