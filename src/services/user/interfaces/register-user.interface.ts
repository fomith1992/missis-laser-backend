export interface RegisterUserParameters {
  userToken: string
  pushToken: string
  phone: string
}
