export interface ResetPasswordParametrs {
  userId: string
  oldPassword: string
  newPassword: string
}
