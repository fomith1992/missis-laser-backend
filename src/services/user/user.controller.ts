import { Body, Controller, Param, Post, Put, UseFilters, UseGuards } from '@nestjs/common'
import {
  ApiBadRequestResponse,
  ApiConflictResponse,
  ApiCreatedResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
} from '@nestjs/swagger'

import { UserRole } from './schemas/user.schema'
import { UserService } from './user.service'
import { ValidationFilter } from '../../shared/filtres/validation.filter'
import { MongoExceptionFilter } from '../../shared/filtres/mongo-exception.filter'

import { RegisterUserDto, UpdateUserDataDto, GetUserDataDto, PublicUserDto, ResetPasswordDto } from './dto'
import { ApiBaseResponse } from '../../shared/api/response.dto'
import { AuthYclientsGuard } from '../../shared/guards/yclients/auth-yclients.guard'
import { GetUserDataByPhoneDto } from './dto/get-data.dto'

@ApiTags('user')
@Controller('user')
@UseFilters(new ValidationFilter(), MongoExceptionFilter)
export class UserController {
  constructor(private userService: UserService) {}

  @Post()
  @ApiOperation({ summary: 'Registeration' })
  @ApiCreatedResponse({ description: 'Registration successful', type: ApiBaseResponse })
  @ApiConflictResponse({ description: 'Duplicate key in collection' })
  register(@Body() user: RegisterUserDto) {
    return this.userService.createUser({ ...user, role: UserRole.User })
  }

  @Post('/data')
  @ApiOperation({ summary: 'Get public user data by username' })
  @ApiOkResponse({ description: 'Returned user data', type: PublicUserDto })
  async getData(@Body() { username }: GetUserDataDto) {
    return await this.userService.getPublicDataByUserName(username)
  }

  @Post('/phone')
  @ApiOperation({ summary: 'Get public user data by phone' })
  @ApiOkResponse({ description: 'Returned user data', type: PublicUserDto })
  async getDataByPhone(@Body() { phone }: GetUserDataByPhoneDto) {
    return await this.userService.getUserByPhone(phone)
  }

  @Put('/data')
  @ApiOperation({ summary: 'Update user info (for yclients users only)' })
  @ApiCreatedResponse({ description: 'User update successful', type: ApiBaseResponse })
  @ApiBadRequestResponse({ description: 'User not found or User data is empty, nothing to update' })
  @UseGuards(AuthYclientsGuard)
  updateData(@Body() user: UpdateUserDataDto) {
    return this.userService.updateUser(user)
  }

  @Post(':userId/password-reset')
  @ApiOperation({ summary: 'Reset/set user password' })
  async resetUserPassword(@Param('userId') userId: string, @Body() body: ResetPasswordDto) {
    return this.userService.resetPassword({
      userId,
      newPassword: body.new,
      oldPassword: body.old,
    })
  }
}
