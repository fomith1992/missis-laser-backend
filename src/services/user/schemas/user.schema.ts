import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose'
import { Document } from 'mongoose'

export type UserDocument = User & Document

export enum Sex {
  NotKnown = 0,
  Male = 1,
  Famele = 2,
}

export enum UserRole {
  User = 'USER',
  Admin = 'ADMIN',
}

@Schema()
export class User {
  @Prop({ unique: true })
  username: string

  @Prop({ default: UserRole.User })
  role: UserRole

  @Prop({ default: Sex.NotKnown })
  sex: Sex

  @Prop()
  password?: string

  @Prop()
  pushToken?: string

  /* Yclients  */

  @Prop()
  userToken?: string

  @Prop()
  phone?: string
}

export const UserSchema = SchemaFactory.createForClass(User)
