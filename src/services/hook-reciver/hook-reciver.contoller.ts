import { BadRequestException, Body, Controller, Post, UseFilters } from '@nestjs/common'
import { ApiExcludeEndpoint, ApiTags } from '@nestjs/swagger'

import { HookReciveDto } from './dto/crud-records.dto'
import { HookReciverService } from './hook-reciver.service'

import { ValidationFilter } from '../../shared/filtres/validation.filter'

@ApiTags('Webhook')
@Controller('/webhook')
@UseFilters(new ValidationFilter())
export class HookReciverController {
  constructor(private hookReciverService: HookReciverService) {}

  @Post()
  @ApiExcludeEndpoint()
  async reciveHook(@Body() params: HookReciveDto) {
    if (params.resource === 'record') {
      return await this.hookReciverService.changedResourceRecord({ ...params, resource: params.resource })
    }

    throw new BadRequestException()
  }
}
