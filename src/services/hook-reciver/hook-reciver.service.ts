import { Injectable } from '@nestjs/common'
import { ScheduleService } from '../../services/schedule/schedule.service'
import { changedResourceRecordParameters } from './interfaces'

@Injectable({})
export class HookReciverService {
  constructor(private readonly ScheduleService: ScheduleService) {}

  async changedResourceRecord(hookRecord: changedResourceRecordParameters) {
    switch (hookRecord.status) {
      case 'create':
        return this.ScheduleService.createRecord({ ...hookRecord, status: hookRecord.status })
      case 'update':
        return this.ScheduleService.updateRecord({ ...hookRecord, status: hookRecord.status })
      case 'delete':
        return this.ScheduleService.deleteRecord({ ...hookRecord, status: hookRecord.status })
    }
  }
}
