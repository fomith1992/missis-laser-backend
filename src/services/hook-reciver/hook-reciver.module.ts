import { Module } from '@nestjs/common'
import { ScheduleModule } from '../../services/schedule/schedule.module'
import { HookReciverController } from './hook-reciver.contoller'
import { HookReciverService } from './hook-reciver.service'

@Module({
  imports: [ScheduleModule],
  controllers: [HookReciverController],
  providers: [HookReciverService],
  exports: [HookReciverService],
})
export class HookReciverModule {}
