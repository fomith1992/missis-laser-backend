import { Type } from 'class-transformer'
import {
  ArrayNotEmpty,
  IsArray,
  IsBoolean,
  IsDefined,
  IsNotEmptyObject,
  IsNumber,
  IsString,
  ValidateNested,
} from 'class-validator'

export class ServiceDto {
  @IsNumber()
  readonly id: number

  @IsString()
  readonly title: string

  @IsNumber()
  readonly cost: number
}

export class ClientDto {
  @IsNumber()
  readonly id: number

  @IsString()
  readonly name: string

  @IsString()
  readonly phone: string
}

export class RecordDto {
  @IsNumber()
  readonly id: number

  @IsNumber()
  readonly company_id: number

  @IsString()
  readonly datetime: string

  @IsString()
  readonly create_date: string

  @IsNumber()
  readonly seance_length: number

  @IsBoolean()
  readonly deleted: boolean

  @IsArray()
  @IsDefined()
  @ArrayNotEmpty()
  @ValidateNested({ each: true })
  @Type(() => ServiceDto)
  readonly services: ServiceDto[]

  @IsDefined()
  @IsNotEmptyObject()
  @ValidateNested()
  @Type(() => ClientDto)
  readonly client: ClientDto
}
