import { Type } from 'class-transformer'
import { IsString, ValidateNested, IsNotEmptyObject, IsDefined, IsNumber, IsEnum } from 'class-validator'

import { RecordDto } from './base.dto'
import { Maybe } from '../../../shared/utilityTypes'

export class HookReciveDto {
  @IsNumber()
  readonly company_id: number

  @IsString()
  readonly resource: Maybe<'record'>

  @IsNumber()
  readonly resource_id: number

  @IsString()
  @IsEnum(['create', 'update', 'delete'])
  readonly status: 'create' | 'update' | 'delete'

  @IsDefined()
  @IsNotEmptyObject()
  @ValidateNested()
  @Type(() => RecordDto)
  readonly data: RecordDto
}
