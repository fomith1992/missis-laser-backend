import { IRecord } from '../../../shared/types'

export interface changedResourceRecordParameters {
  company_id: number
  resource: 'record'
  resource_id: number
  status: 'create' | 'update' | 'delete'
  data: IRecord
}
