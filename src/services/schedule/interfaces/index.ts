import { Dict } from '../../../shared/utilityTypes'
import { IRecord } from '../../../shared/types'

type CRUDTypes = 'create' | 'delete' | 'update'

export interface IChangedRecord<T extends CRUDTypes = CRUDTypes> {
  company_id: number
  resource: 'record'
  resource_id: number
  status: T
  data: IRecord
}

export interface dbRecordSchemaParametrs {
  id: number
  //Record
  datetime: string //UTC
  recordId: number
  companyId: number
  //Client
  clientName: string
  clientPhone: string
  //Notify status
  isAfter: boolean
}

export interface ITransformAfterBeforeRecord {
  after: dbRecordSchemaParametrs
  before: dbRecordSchemaParametrs
}

export interface ITransformRecordToNotification {
  pushToken: string
  data?: Dict<unknown>
}
