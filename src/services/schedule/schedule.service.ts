import { Injectable, UseFilters } from '@nestjs/common'
import { Cron, CronExpression } from '@nestjs/schedule'
import { InjectModel } from '@nestjs/mongoose'
import { Model } from 'mongoose'
import moment from 'moment'

import { RecordDocument, ScheduleRecordMongo } from './schemas/record.schema'

import { Notification } from '../../services/notification/interfaces'

import {
  dbRecordSchemaParametrs,
  IChangedRecord,
  ITransformAfterBeforeRecord,
  ITransformRecordToNotification,
} from './interfaces'

import { NotificationService } from '../notification/notification.service'
import { UserService } from '../user/user.service'

import { ValidationException } from '../../shared/exceptions/validation.exception'
import { ValidationFilter } from '../../shared/filtres/validation.filter'
import { Dict, PartialExcept } from '../../shared/utilityTypes'
import { clearPhone } from '../../shared/common/user'

@Injectable()
@UseFilters(new ValidationFilter())
export class ScheduleService {
  constructor(
    @InjectModel(ScheduleRecordMongo.name) private scheduleMongo: Model<RecordDocument>,
    private notificationService: NotificationService,
    private userService: UserService,
  ) {}

  async createRecord(rawRecord: IChangedRecord<'create'>) {
    const id = this.makeSheduleRecordId(rawRecord)

    await this.validateExistOrNotDbRecord(id, 'notExist')

    const { after, before } = this.transformAfterBeforeRecord(rawRecord)

    await this.dbCreateRecord(before)
    await this.dbCreateRecord(after)

    return { success: 'true', data: [before, after] }
  }

  async updateRecord(rawRecord: IChangedRecord<'update'>) {
    const id = this.makeSheduleRecordId(rawRecord)
    await this.validateExistOrNotDbRecord(id, 'exist')

    const { after, before } = this.transformAfterBeforeRecord(rawRecord)

    await this.dbUpdateRecordById(after, true)
    await this.dbUpdateRecordById(before, false)

    return { success: 'true', data: [after, before] }
  }

  async deleteRecord(rawRecord: IChangedRecord<'delete'>) {
    const id = this.makeSheduleRecordId(rawRecord)
    await this.validateExistOrNotDbRecord(id, 'exist')
    await this.dbDeleteRecordById(id)

    return { success: 'true', data: id }
  }

  private makeSheduleRecordId = (rawRecord: IChangedRecord) => {
    return Number(`${rawRecord.data.id}${rawRecord.company_id}`)
  }

  private transformAfterBeforeRecord(rawRecord: IChangedRecord): ITransformAfterBeforeRecord {
    const rData = rawRecord.data

    const baseRecord = {
      id: this.makeSheduleRecordId(rawRecord),
      recordId: rData.id,
      companyId: rawRecord.company_id,
      clientName: rData.client.name,
      clientPhone: clearPhone(rData.client.phone),
    }

    return {
      before: {
        ...baseRecord,
        datetime: moment(rawRecord.data.datetime).subtract(24, 'hour').utc().format(),
        isAfter: false,
      },

      after: {
        ...baseRecord,
        datetime: moment(rawRecord.data.datetime).add(rawRecord.data.seance_length, 'second').utc().format(),
        isAfter: true,
      },
    }
  }

  private async validateExistOrNotDbRecord(id: number, query: 'exist' | 'notExist') {
    const dbRecord = await this.scheduleMongo.findOne({ id })
    if (dbRecord && query === 'notExist') {
      throw new ValidationException({ message: 'Record already exist' })
    }

    if (!dbRecord && query === 'exist') {
      throw new ValidationException({ message: 'Record not exist' })
    }
  }

  private dbCreateRecord(dbSchema: dbRecordSchemaParametrs) {
    return this.scheduleMongo.create(dbSchema)
  }

  private async dbUpdateRecordById(dbSchema: PartialExcept<dbRecordSchemaParametrs, 'id'>, isAfter: boolean) {
    return this.scheduleMongo.findOneAndUpdate({ id: dbSchema.id, isAfter }, dbSchema, { new: true })
  }

  private async dbDeleteRecordById(id: number) {
    return this.scheduleMongo.deleteMany({ id })
  }

  @Cron(CronExpression.EVERY_30_MINUTES)
  private async checkRecords() {
    this.notifyRecords({ isAfter: true })
    this.notifyRecords({ isAfter: false })
  }

  private async notifyRecords({ isAfter }: { isAfter: boolean }) {
    const now = moment.utc().format()
    const records = await this.scheduleMongo.find({ datetime: { $lte: now }, isAfter })

    const phonesUsers = records.map((record) => record.clientPhone)

    const users = await this.userService.getUsersByPhones(phonesUsers)

    //append data to notifications
    const hashUsersByPhones: Dict<{ pushToken: string; data?: Dict<unknown> }> = {}

    users.forEach((user) => {
      if (user?.phone && user?.pushToken) {
        hashUsersByPhones[user.phone] = { pushToken: user.pushToken }
      }
    })

    records.forEach(({ clientPhone }) => {
      const item = hashUsersByPhones[clientPhone]
      if (item) {
        hashUsersByPhones[clientPhone] = {
          ...item,
          data: { isAfter },
        }
      }
    })

    const notificationsWithData = Object.values(hashUsersByPhones)

    const notifications = isAfter
      ? notificationsWithData.map(this.transformToAfterRecordNotification)
      : notificationsWithData.map(this.transformToBeforeRecordNotification)

    this.notificationService.sendMoreNotifications(notifications)

    // deleting records from db after sending notifications
    const ids = records.map(({ id }) => id)
    await this.scheduleMongo.deleteMany({ id: { $in: ids }, isAfter: false })
  }

  private transformToBeforeRecordNotification({ pushToken, data }: ITransformRecordToNotification): Notification {
    return {
      to: pushToken,
      title: 'Подружки',
      body: 'Встреча будет через 24 часа',
      data,
    }
  }

  private transformToAfterRecordNotification({ pushToken, data }: ITransformRecordToNotification): Notification {
    return {
      to: pushToken,
      title: 'Подружки',
      body: 'Спасибо за посещение. Хотите посмотреть рекомендации после процедуры?',
      data,
    }
  }
}
