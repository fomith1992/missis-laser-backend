import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose'
import { Document } from 'mongoose'

export type RecordDocument = ScheduleRecordMongo & Document

@Schema({ collection: 'schedule' })
export class ScheduleRecordMongo {
  // 'idcompany_id' id concat with company_id for unic identifier
  @Prop()
  id: string

  //Record
  @Prop()
  datetime: string //UTC

  @Prop()
  recordId: number

  @Prop()
  companyId: number

  //Client
  @Prop()
  clientName: string

  @Prop()
  clientPhone: string

  //Notify status
  @Prop()
  isAfter: boolean
}

export const RecordSchema = SchemaFactory.createForClass(ScheduleRecordMongo)
