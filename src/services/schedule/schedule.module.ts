import { Module } from '@nestjs/common'
import { MongooseModule } from '@nestjs/mongoose'

import { NotificationModule } from '../notification/notification.module'
import { UserModule } from '../user/user.module'

import { ScheduleRecordMongo, RecordSchema } from './schemas/record.schema'
import { ScheduleService } from './schedule.service'

@Module({
  imports: [
    MongooseModule.forFeature([{ name: ScheduleRecordMongo.name, schema: RecordSchema }]),
    NotificationModule,
    UserModule,
  ],
  providers: [ScheduleService],
  exports: [ScheduleService],
})
export class ScheduleModule {}
