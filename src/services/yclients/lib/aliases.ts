import { Dict } from '../../../shared/utilityTypes'

export const FrontIconsAlias = {
  ['Все тело']: {
    bodyBodyFull: [
      'Все тело (ноги полностью, глубокое бикини, подмышки, малая зона)',
      'Все тело (малая зона, подмышки, бикини, ноги полностью)',
    ],
    bodyBodyFullAndHands: [
      'Все тело + руки (руки полностью, ноги полностью, глубокое бикини, подмышки, малая зона)',
      'Все тело (малая зона,  подмышки, бикини, ноги полностью)+ руки полностью',
      'Вспышка лазером 1 ед.',
    ],
  },
  ['Лицо и шея']: {
    faceSmallZone: ['Малая зона'],
    faceFaceFull: ['Лицо полностью'],
    faceForeheadOrCheeks: ['Лоб и щеки'],
    faceTempleOrSideburns: ['Виски или бакенбарды или нос'],
    faceNeck: ['Шея (передняя и задняя поверхность)', 'Шея полностью', 'Шея передняя или задняя часть'],
  },
  ['Руки']: {
    handsArmpit: ['Подмышки'],
    handsUpToElbow: ['Руки до локтя'],
    handsAboveElbow: ['Руки выше локтя'],
    handsHandsFull: ['Руки полностью'],
    handsWrist: ['Кисти рук'],
    handsFingers: ['Кисти рук с пальцами', 'Пальцы рук'],
    handsShoulders: ['Плечи'],
  },
  ['Туловище']: {
    torsStomachWhiteLine: ['Живот, белая линия', 'Живот (белая линия)'],
    torsBackFull: ['Ареолы', 'Спина полностью'],
    torsChestFull: ['Грудь полностью'],
    torsNeckline: ['Декольте'],
    torsStomachFull: ['Живот полностью'],
    torsBackLowerThird: ['Спина нижняя треть'],
  },
  ['Бикини']: {
    bikiniBikiniDeep: ['Глубокое бикини', 'бикини глубокое'],
    bikiniBikiniClassic: ['Бикини классическое'],
    bikiniInterBerryZone: ['Межъягодичная зона'],
    bikiniButtocks: ['Ягодицы'],
  },
  ['Ноги']: {
    footsSheensWithKnees: ['Голени с коленями', 'голени'],
    footsLegsFull: ['Ноги полностью'],
    footsHips: ['Бедра (передняя'],
    footsHipsFull: ['Бедра полностью'],
    footsKnees: ['Колени'],
    footsRaisingTheLegs: ['Подъём ног'],
    footsRaisingLegsWithToes: ['Подъём ног с пальцами', 'подъем ног с пальцами'],
    footsToes: ['Пальцы ног'],
  },
}

type THashItem = {
  titles: string[]
  categoryName: string
  imageType: string
}

export const hashTableByImageType = Object.entries(FrontIconsAlias).reduce((acc, [categoryName, zones]) => {
  Object.entries(zones).forEach(([imageType, titles]) => {
    acc[imageType] = { titles, categoryName, imageType }
  })

  return acc
}, {} as Dict<THashItem>)
