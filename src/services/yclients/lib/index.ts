import { IFrontService, IService } from '../../../shared/types'

import { hashTableByImageType } from './aliases'

const lc = (value: string) => value.toLowerCase()

export const makeFrontServices = (service: IService): IFrontService => {
  let categoryName = null
  let imageType = null

  Object.values(hashTableByImageType).some((hashItem) => {
    const isFind = hashItem.titles.some((value) => lc(service.title).includes(lc(value)))
    if (isFind) {
      categoryName = hashItem.categoryName
      imageType = hashItem.imageType
    }

    return isFind
  })

  return { ...service, imageType, categoryName }
}
