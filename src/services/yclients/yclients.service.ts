import { HttpService, Injectable } from '@nestjs/common'

import { makeFrontServices } from './lib'
import { GetServices } from './interfaces/get-services.interface'
import { IService } from '../../shared/types'
import { ValidationException } from '../../shared/exceptions/validation.exception'
import { YClientsException } from '../../shared/exceptions/yclients.exception'
import { VerifyUserParameters } from './interfaces'

@Injectable({})
export class YclientsService {
  private readonly yclientsUrl = process.env.YCLIENTS_URL
  private readonly partnerToken = process.env.PARTNER_TOKEN

  constructor(private readonly HttpService: HttpService) {}

  async getServices({ categoryId, clinicId, userToken }: GetServices) {
    return this.HttpService.get(`${this.yclientsUrl}/company/${clinicId}/services/?category_id=${categoryId}`, {
      headers: {
        Accept: 'application/vnd.yclients.v2+json',
        Authorization: `Bearer ${this.partnerToken}, User ${userToken}`,
      },
    }).toPromise()
  }

  transformServicesToFront(services: IService[]) {
    const clearServices = this.clearServices(services)

    return clearServices.map(makeFrontServices)
  }

  private clearServices(services: IService[]): IService[] {
    return services.map((service) => ({
      id: service.id,
      title: service.title,
      category_id: service.category_id,
      price_min: service.price_min,
      price_max: service.price_max,
      discount: service.discount,
      prepaid: service.prepaid,
    }))
  }

  async verifyYclientsUser({ phone, userToken }: VerifyUserParameters) {
    try {
      const { data } = await this.fetchUserData(userToken)
      const yclientsPhone = data.data.phone

      if (yclientsPhone !== phone) {
        throw new ValidationException({ message: 'Phone number does not match userToken' })
      }
    } catch (error) {
      if (error instanceof ValidationException) {
        throw new ValidationException(error)
      }

      throw new YClientsException('Invalid userToken')
    }

    return true
  }

  private async fetchUserData(user_token: string) {
    return this.HttpService.get(`${this.yclientsUrl}/user/data`, {
      headers: {
        Accept: 'application/vnd.yclients.v2+json',
        Authorization: `Bearer ${this.partnerToken}, User ${user_token}`,
      },
    }).toPromise()
  }
}
