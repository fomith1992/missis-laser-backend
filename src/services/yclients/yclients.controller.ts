import { Controller, Param, Query, Body, UseFilters, Post } from '@nestjs/common'
import { ApiTags } from '@nestjs/swagger'

import { YclientsService } from './yclients.service'
import { UserTokenDto } from './dto/pathner-user-token.dto'

import { ValidationFilter } from '../../shared/filtres/validation.filter'
import { YClientsException } from '../../shared/exceptions/yclients.exception'
@ApiTags('Yclients')
@Controller('/yclients')
@UseFilters(new ValidationFilter())
export class YclientsController {
  constructor(private yclientsService: YclientsService) {}

  @Post('company/:clinicId/services')
  async root(
    @Param('clinicId') clinicId: number,
    @Query('category_id') category_id: number,
    @Body() { userToken }: UserTokenDto,
  ) {
    try {
      const { data: response } = await this.yclientsService.getServices({
        clinicId,
        categoryId: category_id,
        userToken,
      })

      return {
        ...response,
        data: this.yclientsService.transformServicesToFront(response.data),
      }
    } catch (error) {
      throw new YClientsException('Request error')
    }
  }
}
