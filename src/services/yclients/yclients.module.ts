import { Global, HttpModule, Module } from '@nestjs/common'

import { YclientsController } from './yclients.controller'
import { YclientsService } from './yclients.service'

@Global()
@Module({
  imports: [
    HttpModule.register({
      headers: {
        withCredentials: true,
        Accept: 'application/vnd.yclients.v2+json',
      },
      baseURL: 'https://api.yclients.com/api/v1/',
    }),
  ],
  controllers: [YclientsController],
  providers: [YclientsService],
  exports: [YclientsService],
})
export class YclientsModule {}
