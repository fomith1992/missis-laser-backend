import { IFrontService } from '../../../shared/types'

export interface IFrontServiceList {
  title: string
  data: IFrontService
}

export interface VerifyUserParameters {
  userToken: string
  phone: string
}
