export interface GetServices {
  userToken: string
  clinicId: number
  categoryId: number
}
