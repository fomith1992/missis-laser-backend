import { HttpService, Injectable } from '@nestjs/common'

import { Notification } from './interfaces'

import { chunk } from '../../shared/common/array'

const MAX_BATCH_SIZE = 100

@Injectable()
export class NotificationService {
  private readonly expoPushUrl = process.env.EXPO_PUSH_URL

  constructor(private readonly HttpService: HttpService) {}

  async sendMoreNotifications(notifications: Notification[]) {
    if (!notifications.length) return
    const chunkedNotifications = chunk(notifications, MAX_BATCH_SIZE)
    for (const batch of chunkedNotifications) {
      try {
        await this.HttpService.post(`${this.expoPushUrl}/send`, batch).toPromise()
      } catch (error) {
        console.log(error)
      }
    }
  }
}
