import { Body, Controller, Post, UseFilters, UseGuards } from '@nestjs/common'
import { ApiTags } from '@nestjs/swagger'

import { UpdateNotificationTokenDto } from './dto/user.dto'

import { ValidationFilter } from '../../shared/filtres/validation.filter'
import { UserService } from '../../services/user/user.service'
import { AuthYclientsGuard } from '../../shared/guards/yclients/auth-yclients.guard'

@ApiTags('Notification')
@Controller('/notify')
@UseFilters(new ValidationFilter())
export class NotificationController {
  constructor(private userService: UserService) {}

  @Post('/update')
  @UseGuards(AuthYclientsGuard)
  async registerOrUpdateExpoUserToken(@Body() dto: UpdateNotificationTokenDto) {
    return this.userService.setUserExpoToken(dto)
  }
}
