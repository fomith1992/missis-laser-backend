import { Dict } from '../../../shared/utilityTypes'

export interface Notification {
  title: string
  body: string
  to: string
  data?: Dict<unknown>
}
