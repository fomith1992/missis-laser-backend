import { IsNotEmpty, IsString, Matches } from 'class-validator'

export class UpdateNotificationTokenDto {
  @IsNotEmpty()
  @IsString()
  readonly userToken: string

  @IsNotEmpty()
  @IsString()
  readonly pushToken: string

  @IsString()
  @Matches(/^7[\d]{10}$/, { message: 'Invalid phone format' })
  readonly phone: string
}
