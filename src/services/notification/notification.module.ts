import { HttpModule, Module } from '@nestjs/common'
import { NotificationController } from './notification.controller'
import { NotificationService } from './notification.service'

import { UserModule } from '../../services/user/user.module'

@Module({
  imports: [
    HttpModule.register({
      headers: {
        withCredentials: true,
        Accept: 'application/vnd.yclients.v2+json',
      },
      baseURL: 'https://api.yclients.com/api/v1/',
    }),
    UserModule,
  ],
  controllers: [NotificationController],
  providers: [NotificationService],
  exports: [NotificationService],
})
export class NotificationModule {}
