import { Types } from 'mongoose'

export interface PromocodeInterface extends Document {
  key: string
  services: number[]
  activetedPhones: number[]
  landing: Types.ObjectId
}

export interface CreatePromocodeInterface {
  key: string
  services: number[]
  landingType: string
}

export interface ActivatePromocodeInterface {
  key: string
  phone: string
}

export interface CheckInPromocodeInterface {
  key: string
  phone: string
}
