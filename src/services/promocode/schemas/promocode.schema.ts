import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose'
import { Document, Types } from 'mongoose'

import { LandingMongo } from '../../landing/schema/landing.schema'

export type PromocodeDocument = PromocodeMongo & Document

@Schema({ collection: 'promocodes' })
export class PromocodeMongo {
  @Prop({ required: true, unique: true })
  key: string

  @Prop({ required: true, type: Types.ObjectId, ref: LandingMongo.name })
  landingId: Types.ObjectId

  @Prop({ required: true })
  services: number[]

  @Prop({ default: [] })
  activetedPhones: number[]
}

export const PromocodeSchema = SchemaFactory.createForClass(PromocodeMongo)
