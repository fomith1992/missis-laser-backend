import { Body, Controller, Post, UseFilters, UseGuards } from '@nestjs/common'
import { ApiTags } from '@nestjs/swagger'

import { PromocodeService } from './promocode.service'

import { MongoExceptionFilter } from '../../shared/filtres/mongo-exception.filter'
import { AuthYclientsGuard } from '../../shared/guards/yclients/auth-yclients.guard'
import { ValidationFilter } from '../../shared/filtres/validation.filter'

import { PromocodeCheckDto, PromocodeCheckDtoResponse } from './dto'
import { PromocodeActivateDto, PromocodeActivateDtoResponse } from './dto/promocode-activate.dto'

@ApiTags('Promocode')
@Controller('/promocode')
@UseFilters(MongoExceptionFilter, new ValidationFilter())
@UseGuards(AuthYclientsGuard)
export class PromocodeController {
  constructor(private promocodeService: PromocodeService) {}

  @Post('/check')
  async check(@Body() { key, phone }: PromocodeCheckDto): Promise<PromocodeCheckDtoResponse> {
    return await this.promocodeService.checkIn({ key, phone })
  }

  @Post('/activate')
  async activatePromocode(@Body() { key, phone }: PromocodeActivateDto): Promise<PromocodeActivateDtoResponse> {
    return await this.promocodeService.activate({ key, phone })
  }
}
