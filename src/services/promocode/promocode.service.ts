import { Injectable } from '@nestjs/common'
import { InjectModel } from '@nestjs/mongoose'
import { Model } from 'mongoose'

import { ActivatePromocodeInterface, CreatePromocodeInterface, CheckInPromocodeInterface } from './interfaces'
import { PromocodeMongo, PromocodeDocument } from './schemas/promocode.schema'

import { BaseException } from '../../shared/exceptions/base.exception'
import { LandingService } from '../../services/landing/landing.service'
import { clearPhone } from '../../shared/common/user'

@Injectable()
export class PromocodeService {
  constructor(
    @InjectModel(PromocodeMongo.name) private promocodeMongo: Model<PromocodeDocument>,
    private landingService: LandingService,
  ) {}

  async checkIn({ key, phone }: CheckInPromocodeInterface) {
    const promocode = await this.promocodeMongo.findOne({ key })
    if (!promocode) throw new BaseException({ message: 'Promocode not exist' })

    await this.canUse(promocode.activetedPhones, clearPhone(phone))

    const landing = await this.landingService.getLandingById(promocode.landingId)
    if (!landing) throw new BaseException({ message: 'Landing was removed!' })

    return {
      success: true,
      data: { services: promocode.services, landing },
    }
  }

  async activate({ key, phone }: ActivatePromocodeInterface) {
    const promocode = await this.promocodeMongo.findOne({ key })
    if (!promocode) throw new BaseException({ message: 'Promocode not exist' })

    await this.promocodeMongo.updateOne({ key }, { $addToSet: { activetedPhones: Number(clearPhone(phone)) } })

    return { success: true }
  }

  //TODO ADMIN
  async create({ landingType, ...promocode }: CreatePromocodeInterface) {
    const landing = await this.landingService.getLandingByType(landingType)

    if (landing) {
      const createdPromocode = new this.promocodeMongo({ ...promocode, landingId: landing._id })
      return createdPromocode.save()
    }

    throw new BaseException({ message: `Landing with type: "${landingType}". Not found.` })
  }

  //TODO ADMIN
  async reset(key: string) {
    const doc = await this.promocodeMongo.findOne({ key })
    if (doc) {
      await this.promocodeMongo.updateOne({ key }, { activetedPhones: [] })
      return { success: true }
    }

    throw new BaseException({ message: `Promocode with key: "${key}". Not found.` })
  }

  private async canUse(activetedPhones: number[], userPhone: string) {
    const isActivated = activetedPhones.some((dbPhone) => dbPhone === Number(userPhone))

    if (isActivated) {
      throw new BaseException({ message: 'You have already activated' })
    }
  }
}
