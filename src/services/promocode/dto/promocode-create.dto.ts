import { IsNotEmpty, IsString, IsNumber, ArrayNotEmpty } from 'class-validator'

export class PromocodeCreateDto {
  @IsNotEmpty()
  @IsString()
  key: string

  @ArrayNotEmpty()
  @IsNumber({}, { each: true })
  services: number[]

  @IsNotEmpty()
  @IsString()
  landingType: string
}
