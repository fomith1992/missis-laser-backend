import { IsNotEmpty, IsString } from 'class-validator'

import { LandingDto } from '../../landing/dto/landing.dto'
import { AuthBodyDto } from '../../../shared/guards/yclients/auth-body.dto'

export class PromocodeCheckDto extends AuthBodyDto {
  @IsNotEmpty()
  @IsString()
  key: string
}

class PromocodeDataDtoResponse {
  services: number[]

  landing: LandingDto
}

export class PromocodeCheckDtoResponse {
  success: boolean

  data: PromocodeDataDtoResponse
}
