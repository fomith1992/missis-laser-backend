import { IntersectionType } from '@nestjs/swagger'

import { AuthBodyDto } from '../../../shared/guards/yclients/auth-body.dto'
import { PromocodeCheckDto } from './promocode-check.dto'

export class PromocodeActivateDto extends IntersectionType(PromocodeCheckDto, AuthBodyDto) {}

export class PromocodeActivateDtoResponse {
  success: boolean
}
