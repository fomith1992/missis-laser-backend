import { Module } from '@nestjs/common'
import { MongooseModule } from '@nestjs/mongoose'

import { PromocodeMongo, PromocodeSchema } from './schemas/promocode.schema'
import { PromocodeService } from './promocode.service'
import { PromocodeController } from './promocode.controller'

import { UserModule } from '../../services/user/user.module'
import { LandingModule } from '../landing/landing.module'

@Module({
  imports: [
    MongooseModule.forFeature([{ name: PromocodeMongo.name, schema: PromocodeSchema }]),
    UserModule,
    LandingModule,
  ],
  controllers: [PromocodeController],
  providers: [PromocodeService],
  exports: [PromocodeService],
})
export class PromocodeModule {}
