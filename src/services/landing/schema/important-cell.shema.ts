import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose'

@Schema()
export class ImportantCell {
  @Prop({ required: true })
  icon: string

  @Prop({ required: true })
  title: string

  @Prop({ required: true })
  description: string
}

export const ImportantCellSchema = SchemaFactory.createForClass(ImportantCell)
