import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose'
import { Document } from 'mongoose'

import { ImportantCellInteface } from '../interfaces'
import { ImportantCellSchema } from './important-cell.shema'

export type LandingDocument = LandingMongo & Document

@Schema({ collection: 'landings' })
export class LandingMongo {
  @Prop({ require: true, unique: true })
  type: string

  @Prop()
  image: string

  @Prop()
  headline: string

  @Prop()
  description: string

  @Prop({ type: [ImportantCellSchema], default: [] })
  importantCells: ImportantCellInteface[]

  @Prop({ default: [] })
  benefits: string[]

  @Prop({ required: true })
  middleDescription: string

  @Prop({ default: [] })
  indications: string[]
}

export const LandingSchema = SchemaFactory.createForClass(LandingMongo)
