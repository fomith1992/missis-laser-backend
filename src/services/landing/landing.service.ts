import { Injectable } from '@nestjs/common'
import { InjectModel } from '@nestjs/mongoose'
import { Model, Types } from 'mongoose'

import { LandingMongo, LandingDocument } from './schema/landing.schema'

import { LandingInteface } from './interfaces'

@Injectable()
export class LandingService {
  constructor(@InjectModel(LandingMongo.name) private landingMongo: Model<LandingDocument>) {}

  createLanding(landing: LandingInteface) {
    return this.landingMongo.create(landing)
  }

  async getLandingByType(type: string) {
    return await this.landingMongo.findOne({ type })
  }

  async getLandingById(_id: Types.ObjectId) {
    const projection = { _id: 0, __v: 0, 'importantCells._id': 0 }

    return await this.landingMongo.findOne({ _id }, projection)
  }
}
