import { Controller, UseFilters } from '@nestjs/common'
import { ApiTags } from '@nestjs/swagger'

import { MongoExceptionFilter } from '../../shared/filtres/mongo-exception.filter'
import { ValidationFilter } from '../../shared/filtres/validation.filter'

import { LandingService } from './landing.service'

@ApiTags('Landing')
@Controller('/landing')
@UseFilters(new ValidationFilter(), MongoExceptionFilter)
export class LandingController {
  constructor(private landingService: LandingService) {}
}
