import { Module } from '@nestjs/common'
import { MongooseModule } from '@nestjs/mongoose'

import { LandingSchema, LandingMongo } from './schema/landing.schema'
import { LandingService } from './landing.service'
import { LandingController } from './landing.controller'

@Module({
  imports: [MongooseModule.forFeature([{ name: LandingMongo.name, schema: LandingSchema }])],
  controllers: [LandingController],
  providers: [LandingService],
  exports: [LandingService],
})
export class LandingModule {}
