export interface ImportantCellInteface {
  icon: string
  title: string
  description: string
}

export interface LandingInteface {
  type: string
  image: string
  headline: string
  description: string
  importantCells: ImportantCellInteface[]
  benefits: string[]
  middleDescription: string
  indications: string[]
}
