import { Type } from 'class-transformer'
import { ArrayNotEmpty, IsArray, IsString, ValidateNested } from 'class-validator'

export class ImportantCellDto {
  @IsString()
  icon: string

  @IsString()
  title: string

  @IsString()
  description: string
}

export class LandingDto {
  @IsString()
  type: string

  @IsString()
  image: string

  @IsString()
  headline: string

  @IsString()
  description: string

  @IsString()
  middleDescription: string

  @IsArray()
  @ArrayNotEmpty()
  @IsString({ each: true })
  benefits: string[]

  @IsArray()
  @ArrayNotEmpty()
  @IsString({ each: true })
  indications: string[]

  @IsArray()
  @ArrayNotEmpty()
  @ValidateNested({ each: true })
  @Type(() => ImportantCellDto)
  readonly importantCells: ImportantCellDto[]
}
