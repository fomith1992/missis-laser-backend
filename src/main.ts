import { HttpStatus, ValidationPipe } from '@nestjs/common'
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger'
import { NestFactory } from '@nestjs/core'
import basicAuth from 'express-basic-auth'

import { AppModule } from './app.module'

import { NotificationModule } from './services/notification/notification.module'
import { YclientsModule } from './services/yclients/yclients.module'
import { PromocodeModule } from './services/promocode/promocode.module'
import { AdminModule } from './services/admin/admin.module'

import { UserModule } from './services/user/user.module'
import { ValidationException } from './shared/exceptions/validation.exception'
import { buildValidationError } from './shared/pipes/validation.pipe'

async function bootstrap() {
  try {
    const PORT = process.env.PORT || 5001
    const app = await NestFactory.create(AppModule)

    app.enableCors()
    app.useGlobalPipes(
      new ValidationPipe({
        whitelist: true,
        errorHttpStatusCode: HttpStatus.NOT_ACCEPTABLE,
        exceptionFactory: (errors) => {
          if (errors.length)
            throw new ValidationException({
              message: 'Validation request failed',
              errors: buildValidationError(errors),
            })
        },
      }),
    )

    if (process.env.NODE_ENV === 'dev') {
      if (process.env.SWAGGER_USER && process.env.SWAGGER_PASSWORD) {
        app.use(
          ['/api/admin'],
          basicAuth({
            challenge: true,
            users: {
              [process.env.SWAGGER_USER]: process.env.SWAGGER_PASSWORD,
            },
          }),
        )
      }

      /* MAIN DOCUMENT  */
      const mainConfig = new DocumentBuilder().setTitle('Alex Laser').setVersion('1.0').build()
      const mainDocument = SwaggerModule.createDocument(app, mainConfig, {
        include: [NotificationModule, YclientsModule, PromocodeModule, UserModule],
      })
      SwaggerModule.setup('api', app, mainDocument)

      /* ADMIN DOCUMENT  */
      const adminConfig = new DocumentBuilder()
        .setTitle('Alex Laser')
        .setDescription('All Private Route for authiticated users')
        .addSecurity('basic', { type: 'http', scheme: 'basic' })
        .setVersion('1.0')
        .build()
      const adminDocument = SwaggerModule.createDocument(app, adminConfig, {
        include: [AdminModule],
      })
      SwaggerModule.setup('api/admin', app, adminDocument)
    }

    await app.listen(PORT, () => console.log(`server started on PORT ${PORT}`))
  } catch (error) {
    console.log(error)
  }
}

bootstrap()
